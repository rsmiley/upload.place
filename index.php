<?php
# index.php
# love-sbb.com Homepage | Written by Roger Smiley
session_start();

/* Classes and their variables */
require_once("inc/cms.class.php");
require_once("inc/users.class.php");
require_once("inc/uploads.class.php");
$cms = new webcms();
$users = new users();
$uploader = new uploader();

/* Script Settings */
$settings['style'] = 'beta';

# This is the file based page system
    switch($_GET['page']) {
        case 'upload':
          $uploader->upload($_POST['api'], $_FILES['f']);
          die();
        break;
        case 'download':
          header("Content-Type: text/x-shellscript");
          header('Content-Disposition: attachment; filename="myupload"');
          readfile('resources/myupload');
          die();
        break;

        case 'login':
          if($users->isLoggedIn()) { $users->logout(); header('Location: '.$_SERVER['REQUEST_URI']); } # Prevent any double logging or mistakes.
            if(isset($_POST['email'])) {
              # We are trying to login
              if(!$users->userExists(explode('@', $_POST['email'])['0'])) { return $cms->errorPage("You cannot login to an account that doesn't exist."); }
              $users->generateSignOnLink(explode('@', $_POST['email'])['0'], 'login');
            } else {
              $users->login($_GET['u'], $_GET['t']);
            }
          header('Location: /');
        break;
        case 'register':
          if($users->isLoggedIn()) { $users->logout(); header('Location: '.$_SERVER['REQUEST_URI']); } # Prevent any double logging or mistakes.
          if(isset($_POST['email'])) {
            # We're creating an account.
            $users->createUser($_POST['email']);
            header('Location: /');
            $page = 'home.php'; # This is purely to prevent errors should Header errors arise.
          } elseif(isset($_GET['t'])) {
            # We're trying to login!!!
            $users->login($_GET['u'], $_GET['t']);
            header('Location: /');
            $page = 'home.php'; # This is purely to prevent errors should Header errors arise.
          } else {
            $page = 'home.php'; # Eventually register.php
          }
        break;
        case 'logout':
        $users->logout();
        header('Location: /');
        break;
            
      default:
      $page = 'home.php';
      break;
    }
# This is the file based page system


# If database is active, this is the dynamic page system
if($page == 'home.php' && ($cms->dbactive() === TRUE && $cms->pageExist($_GET['page']))) {
  # The logic behind this is that if $page is still home, they're either hpmepage or db driven
  #$cms->adminWorld();
  $page = 'temp.php';
}
# If database is active, this is the dynamic page system

#die('CMS Disabled at this time');
###########################################################
# Below will house the actual construction of the page    #
###########################################################

if(file_exists('style/'.$settings['style'].'/header.php')) {
  require_once("style/{$settings['style']}/header.php");
} else {
  die('Style header is invalid, please be sure the headers named <b>header.php</b>');
}
if(file_exists("pages/{$page}")) {
  require_once("pages/{$page}");
} else {
  create_error('<b>We could not find the file you are looking for, I wonder if it even exists.</b>');
}

if(file_exists('style/'.$settings['style'].'/footer.php')) {
  require_once("style/{$settings['style']}/footer.php");
} else {
  die('Style footer is invalid, please be sure the footer named <b>footer.php</b>');
}
?>
