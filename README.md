# Upload.Place - Easy Screenshots/Uploads on Linux
This is the source code for [Upload.Place](https://upload.place/) - a free script that allows you the ability to take screenshots and upload files from your desktop!
It came together as I wanted a method to quickly screenshot and upload my clipboard without any second screens or clicks. I wanted to do my paste/screenshot, get the URL, and be done. This is that simple.

The script itself is written in bash with its backend is a PHP script with a customizable CMS system. You can add pages through the database, using files themselves, or even setup a function for a direct URL resource. 

## PHP Script Requirements
- PHP 5.6/7.x
- MySQL Database
- Database Connection: **inc/db.php**
- Updating of the following files due to hardlinks [CTRL+F "upload.place"]
 - inc/uploads.class.php
 - inc/users.class.php
 - resources/myupload
 - style/header.php
 - style/footer.php

## Install Instructions
- Upload files
- Create database and database user
- Import uploadplace-db.sql
- Update inc/db.php
- Update the URLs in the listed files
- All set!


## Change allowed upload files/filetypes in _inc/uploads.class.php_
```
$this->bannedFiles = array(
			'.htaccess',
			'.user.ini',
			'php.ini'
		);
		$this->bannedExtentions = array(
			'php'
		);
```

## Thank you to included library - [meekrodb](https://github.com/SergeyTsalkov/meekrodb)
- https://github.com/SergeyTsalkov/meekrodb