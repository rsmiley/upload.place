		<script>
		var originalData = $("#formBox").html();
		$("#go").click(function() {
			var emailaddr = $('#email').val();
			$(this).prop("disabled", true).prop('value', "Please Wait!");
		  $.post( "/register/", { email:  emailaddr })
           .done(function( data ) {
           	$("#goBox").empty();
           	$("#goBox").html('<b style="text-align:center; font-size:14px">' + data + '</b>');
           });
		});
		$('#instructions').click(function() {
			$("#formBox").empty().html(" \
				<center> \
				<b>You'll want to run this one liner to install it</b><br /> \
				<textarea style='width:95%;'>wget https://upload.place/download/; mv index.html myupload; chmod +x myupload; sudo mv myupload /usr/local/bin/myupload</textarea></center>" + originalData);
			});
		</script>

</body>	
</html>