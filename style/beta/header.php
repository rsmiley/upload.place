	<?php $base_url = 'https://upload.place/style/'.basename(dirname(__FILE__)).'/'; ?>

<!doctype html>
<html lang="en-US">
<head>

	<meta charset="utf-8">

	<title>Upload Place</title>

	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>
