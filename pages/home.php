	<div id="login">
		<h2 style='text-align: center;'><span class="fa fa-lock"></span> Welcome to Upload Place!<br />
		<p style='font-size:12px; text-align: center;'>Just enter your email and hit sign in. <br />You'll need your API Key in /home/$user/.myupload<br /><br /><a href='/download/'>[ Download ]</a> - <a href='#' id='instructions'>[ Instructions/Install ]</a></p></h2>
		<form action="/register/" method="POST">

			<fieldset>
					<div id='formBox'>
				<?php
					if($users->isLoggedIn()) {
						$email = "value='{$_SESSION['profile']['email']}' readonly disabled";
						$token = $_SESSION['profile']['apitoken'];
						$gobox = "<div style='text-align:center;'><b>Welcome! You're currently signed in!</b><br /><a href='/logout/'>Logout?</a></div>";
					} else {
						$email = "placeholder='rsmiley@upload.place'";
						$token = "Available after login!";
						$gobox = "<div style='text-align:center;' id='goBox'><input type='button' id='go' value='Sign In'></div>";
					}
					//wget https://upload.place/download/; mv index.html myupload; chmod +x myupload; sudo mv myupload /usr/local/bin/myupload
				?>
				<p><label for="email">E-mail address</label></p>
				<p><input type="email" id="email" <?php echo $email; ?>></p>

				<p><label for="password">API Token</label></p>
				<p><input type="text" id="apitoken" value="<?php echo $token; ?>" disabled readonly></p>

				<?php echo $gobox; ?>
				</div>
			</fieldset>

		</form>
	</div> <!-- end login -->
