<?php
/* users.class.php
Manages all things user related
Author: Roger Smiley
*/
class users extends webcms {
	function __construct() {
		# We don't really need anything yet
	}

	public static function userExists($user) {
		$check = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `user` = %s", $user);
		if(is_null($check)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public static function getUser($user) {
		# Full user array
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getUser in users.class.php"); }
		$get = DB::queryFirstRow("SELECT * FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get;
		} else {
			return FALSE;
		}
	}

	public static function getEmail($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getEmail in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `email` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['email'];
		} else {
			return FALSE;
		}
	}

	public static function getRank($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getRank in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `rank` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['rank'];
		} else {
			return FALSE;
		}
	}

	public static function getToken($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getToken in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `token` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['token'];
		} else {
			return FALSE;
		}
	}

	public static function getApiToken($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getApiToken in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `apitoken` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['apitoken'];
		} else {
			return FALSE;
		}
	}

	public static function createUser($email) {
		# Already registered?
		$regCheck = DB::queryFirstRow("SELECT `user`FROM `users` WHERE `email` = %s", $email);
		if(!is_null($regCheck)) { 
			# They have an account already, lets log them in.
			self::generateSignOnLink($regCheck['user'], 'login');
			echo "We've sent you a sign on link. <br />Check your email!";
			exit();
		}

		# We made it this far, we must know them.
		$username = trim($email);
		DB::insert('users', array(
			'user' => $username,
			'email' => $email,
			'activated' => 0
			));
		$emailMessage = "Your registration has been confirmed, but in order to continue you must verify your email address.

Simply click the link below to activate and login to your new account!

".self::generateSignOnLink($username, 'register');
		self::sendMail($email, "Upload Place - Account Created", $emailMessage);
		echo "We've sent you your first login URL!";
		exit();
	}

	public static function login($user, $token) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for login() in users.class.php"); }		
		$compare = DB::queryFirstRow("SELECT `token`,`user` FROM `tokenUrls` WHERE `token` = %s", $token);
		if(is_null($compare)) { return parent::errorPage('Invalid Token'); }
		if($token != $compare['token'] || $user != $compare['user']) { return parent::errorPage("Your tokens don't match up there buddy."); }
		DB::delete('tokenUrls', 'token = %s', $token);

		if(self::getUser($user)['activated'] == 0) {
			# Users by default are not activated to prevent MichaelR from being a l33th4x0r
			# If they're not activated, activate them and run the creation steps.
			DB::update('users', array(
				'activated' => 1), "`user` = %s", $user);
			self::createUserProcess($user);
		}

		# Tokens match, lets give them a welcome token
		$newToken = self::generateToken();
		self::updateUser($user, array('token', 'activated'), array($newToken, '1'));
		setcookie("uploadToken", $newToken, strtotime('+60 days'), '/');
	}

	public static function logout() {
		setcookie('uploadToken', '', strtotime('-1 year'), '/');
		unset($_SESSION['profile']);
	}

	public static function isLoggedIn() {
		if(isset($_SESSION['profile'])) {
			return TRUE;
		} else {
			if(isset($_COOKIE['uploadToken'])) {
				# Question the token.
				$find = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `token` = %s", $_COOKIE['uploadToken']);
				if(is_null($find)) {
					# We did not find any matching token. Kill the cookie!
					# I know we shouldn't suppress errors, but it's gonna send errors until it loads before the html which will eventually happen.
					@self::logout();
					return FALSE;
				} else {
					# $_SESSION['profile'] wasn't set, that's how we got here. Let's build it.
					$getUser = self::getUser($find['user']);
					foreach($getUser as $k => $v) {
						$_SESSION['profile'][$k] = $v;
					}
					return TRUE;
				}
			} else {
				# We have no upload token cookie, not logged in
				return FALSE;
			}
		}
	}

	protected static function updateUser($user, $row, $val) {
		# This expects both $row and $val to be arrays.
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for updateUser in users.class.php"); }		
		$columns = DB::columnList('users');
	    $updates = array_combine($row, $val);
	    foreach($updates as $r => $k) {
	      if(!in_array($r, $columns)) { return parent::errorPage("You are trying to update an invalid column."); }
	        DB::update("users", array(
	      $r => $k), "`user` = %s", $user);
	    }
	}

	protected static function generateToken() {
		/* Stolen from StackOverflow but idr the post */
		$source = file_get_contents('/dev/urandom', false, null, null, 64);
        $source .= uniqid(uniqid(mt_rand(0, PHP_INT_MAX), true), true);
        for ($t=0; $t<64; $t++) {
            $source .= chr((mt_rand() ^ mt_rand()) % 256);
        }
        $return = sha1(hash('sha512', $source, true));
        return $return;
	}

	public static function generateSignOnLink($user, $type) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist to email."); }

		switch($type) {
			case 'login':
				# Login
				# Create url, send email, and return that we did it
				$token = self::generateToken();
				DB::insert('tokenUrls', array(
					'user' => $user,
					'type' => 'login',
					'token' => $token,
					'time' => time()
					));
				$url = "https://upload.place/login/?t={$token}&u={$user}";
				$message = "Your login url is below! Please use it to login to Upload Place!
$url";
				self::sendMail(self::getEmail($user), "Upload Place - Login URL", $message);
			break;

			case 'register':
				# Register
				# Provide the URL for the email and return it
				$token = self::generateToken();
				DB::insert('tokenUrls', array(
					'user' => $user,
					'type' => 'register',
					'token' => $token,
					'time' => time()
					));
				$url = "https://upload.place/register/?t={$token}&u={$user}";
				return $url;
			break;
		}
	}

	protected static function generateSecureUploadToken($user) {
		# This just needs to be complex enough to be secure, but difficult enough that it won't be fun to share.
		return self::generateToken(); # It's quite secure anyway.
	}

	protected static function createUserProcess($user) {
		# Setup their Uploads path
		# Again, I know I shouldn't suppress errors, but testing means the folder already exists. Leave me alone.
		@mkdir('uploads/'.$user, 0755);
		if(is_dir('uploads/'.$user)) {
			$dirCreated = 1;
			# Add the firewall rules to their upload folder.
			$rules = ""; # HTACCESS RULES FOR THE USERS UPLOAD FOLDER; ;removed private info
			file_put_contents('uploads/'.$user.'/.htaccess', $rules);
		} else {
			return FALSE; # We failed a step
		}

		# Assign the user an API token
		self::updateUser($user, array('apitoken'), array(self::generateToken()));

		# Since FTP isn't quite yet something we want to do, pause.
		$FTPcreated = 1;

		if($dirCreated == 1 && $FTPcreated == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


}



?>