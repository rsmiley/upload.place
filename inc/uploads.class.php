<?php

class uploader extends webcms {
	public function __construct() {
		# We don't expect authentication for uploading....
		$this->bannedFiles = array(
			'.htaccess',
			'.user.ini',
			'php.ini'
		);
		$this->bannedExtentions = array(
			'php'
		);
	}

	public function upload($token, $file) {
		# Simple Upload function but verify token
		$user = $this->getUserFromToken($token);
		# We did not find any users with a matching token. Someone would've matched in the db lookup we did.
		if(empty($user)) {
			unlink($_FILES['f']['tmp_name']); # Delete the temp file.
			unset($_FILES);
			echo "We encountered a token mismatch in your upload.";
			return;
		}
		
		if(in_array($file['name'], $this->bannedFiles) || in_array(array_reverse(explode('.', $file['name']))['0'], $this->bannedExtentions)) {
			echo "You tried to upload a forbidden file";
			return;
		}

		if(is_uploaded_file($file['tmp_name'])) {
		move_uploaded_file($file['tmp_name'], 'uploads/'.$user.'/'.$file['name']);
		echo "https://upload.place/uploads/{$user}/{$file['name']}";
		} else {
			echo "Error encountered during upload";
		}
		return;
	}

	public function delete($user, $file) {

	}

	public function logUpload($user, $file) {
		$time = time();
	}

	public function getUserFromToken($token) {
		$get = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `apitoken` = %s", $token);
		if(!is_null($get)) {
			return $get['user'];
		} else {
			return FALSE;
		}
	}

}

?>