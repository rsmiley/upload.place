<?php

class webcms {

  public function __construct() {
    if(file_exists('inc/db.php')) {
    include "inc/db.php";
      if($dbCon['enabled'] === TRUE) {
        include "inc/dbClass.php";
        $this->db = new DB();
        DB::$user = $dbCon['username'];
        DB::$password = $dbCon['password'];
        DB::$dbName = $dbCon['dbname'];
        #if(class_exists('DB')) { echo "DB is active <br />"; }
      } else {
        # We have database connection details, but they're not enabled.
        #echo "Database Connection details exist, but they're not enabled";
        return FALSE;
      }
    } else {
      # We have no database connection details, no sense moving forward
      #echo "Database is disabled";
      return FALSE;
    }

    # Lets not forget to set the fucking time zone
    date_default_timezone_set('America/Denver');
  }

  public static function dbactive() {
    if(class_exists('DB')) { return TRUE; } else { return FALSE; }
  }

  public static function adminWorld() {
    if($this->dbactive()) {
      global $settings;
      $settings['style'] = 'osrs2go-admin';
    } else {
      # set a page to alert that there's no admin panel without a database
      $this->errorPage('There can be no admin panel with no database connection.');
    }
  }

  public static function errorPage($msg) {
    $_SESSION['error_message'] = $msg;
    require_once('pages/error.php');
    require_once('style/master/footer.php');
    exit();
  }

  public static function test() {
    $this->getPage(1);
  }
  
  public static function pageExist($id) {
     $ck = DB::query("SELECT * FROM `pages` WHERE `id` = %i or `crumb` = %s", $id, $id);
    if(count($ck) == 0) { return FALSE; } else { return TRUE; }
  }
  
  public static function getPage($id) {
    if(!$this->dbactive()) { $this->errorPage('No active database'); }
    if(!$this->pageExist($id)) { $this->errorPage('Page does not exist'); } else {
         $ck = DB::query("SELECT * FROM `pages` WHERE `id` = %i or `crumb` = %s", $id, $id);
          return $ck;
    }
  }
  public static function pageLoginOnly($id) {
    if(!$this->dbactive()) { $this->errorPage('No active database'); }
    if(!$this->pageExist($id)) { $this->errorPage('Page does not exist'); } else {
      $q = DB::queryFirstRow("SELECT `loggedinonly` FROM pages WHERE `id` = %i or `crumb` = %s", $id, $id);
        if($q['loggedinonly'] == 1) {
          return TRUE;
        } else {
          return FALSE;
        }
    }
  }

  public static function successMessage($msg) {
    echo '<div class="row">
            <div class="col-lg-12"><div class="alert alert-success" style="border-radius:8px;">
            <button type="button" aria-hidden="true" class="close">×</button>
            <span>'.$msg.'</span>
          </div></div></div>';
  }

  protected static function sendMail($to, $subject, $message) {
    mail($to, $subject, $message);
  }

}

?>
